import os
import pandas as pd
import json
import xml
from xml.etree import ElementTree as ET
import sqlite3


### Converting between file's format function
### 2 parameters: input file path, ouput file format
def file_converter(file_path: str, out_format: str):
    # Return a tuple with root and extension if explicit, of the source
    file_format = os.path.splitext(file_path)[1]

    # For each file_format type we create a df
    if file_format == ".xlsx":
        df = pd.read_excel(file_path)

    elif file_format == ".csv":
        df = pd.read_csv(file_path)

    elif file_format == ".json":
        # Load JSON content from file
        with open(file_path, 'r') as f:
            data = json.load(f)
        # Normalize JSON data into DataFrame
        df = pd.DataFrame.from_dict(pd.json_normalize(data), orient='columns')
    
    elif file_format == ".xml":
        # Parse XML from the file
        tree = ET.parse(file_path)
        root = tree.getroot()

        # Initialize list to store data
        headers = []
        data = []

        # Extract headers and data from XML elements
        for item in root:
            headers.append(item.tag)
            data.append(item.text)
        
        # Create DataFrame
        df = pd.DataFrame([data], columns=headers)
        # data = []
        # for row in root.findall('row'):
        #     item = {}
        #     for child in row:
        #         item[child.tag] = child.text
        #     data.append(item)
        # df = pd.DataFrame(data)

    elif file_format == ".sql":
        with open(file_path, "r") as sql_file:
            sql = sql_file.read()
        conn = sqlite3.connect(':memory:')
        cursor = conn.cursor()
        cursor.executescript(sql)
        conn.commit()
        sql = "SELECT * FROM sqlite_master WHERE type='table';"
        cursor.execute(sql)
        table_name = cursor.fetchone()[1]
        df = pd.read_sql_query(f"SELECT * FROM {table_name}", conn)
        conn.close()

    elif file_format == '.db':
        conn = sqlite3.connect(file_path)
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table_name = cursor.fetchone()[0]
        df = pd.read_sql_query(f"SELECT * FROM {table_name}", conn)
        conn.close()
    

    # Test
    print(df)

    # Convert the df into the desired file format (out_format)
    output_file = os.path.split(file_path)[0] + "converted." + out_format

    if out_format == ".xlsx":
        df.to_excel(output_file, index= False)

    elif out_format == ".csv":
        df.to_csv(output_file, index = False)

    elif out_format == ".json":
        df.to_json(output_file,orient='records')

    elif out_format == ".xml":
        df.to_xml(output_file)

    elif out_format == ".sql":
        output_file = os.path.split(file_path)[0] + "converted." + "db"
        conn = sqlite3.connect(output_file)
        df.to_sql('table_A', conn, if_exists='replace', index=False)
        conn.close()


